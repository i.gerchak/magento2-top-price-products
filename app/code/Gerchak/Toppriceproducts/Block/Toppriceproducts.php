<?php
namespace Gerchak\Toppriceproducts\Block;

class Toppriceproducts extends \Magento\Catalog\Block\Product\AbstractProduct
{
    protected $helperData;
    protected $productCollectionFactory;
    protected $productStatus;
    protected $productVisibility;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Gerchak\Toppriceproducts\Helper\Data $helperData,
        array $data = []
    )
    {
        $this->helperData = $helperData;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        return parent::__construct($context, $data);
    }

    /**
     * @return Module Status
     */
    public function isEnable()
    {
        return $this->helperData->getGeneralConfig('enable');
    }

    /**
     * @return Module Title
     */
    public function getTitle()
    {
        return $this->helperData->getGeneralConfig('title');
    }


    /**
     * @return Module Title Info
     */
    public function getTitleInfo()
    {
        return $this->helperData->getGeneralConfig('info');
    }

    /**
     * @return Product Collection
     */
    public function getProductCollection()
    {
        $show_items = $this->helperData->getGeneralConfig('show_items');

        $collection = $this->productCollectionFactory->create();
        $collection
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('final_price')
            ->addAttributeToSelect('special_price')
            ->addAttributeToSelect('special_from_date')
            ->addAttributeToSelect('sale')
            ->addAttributeToSelect('image')
            ->addAttributeToSelect('small_image')
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToSort('price', 'DESC')
            ->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()])
            ->setVisibility($this->productVisibility->getVisibleInSiteIds())
            ->setPageSize($show_items);

        return $collection;
    }
}